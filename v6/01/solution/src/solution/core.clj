(ns solution.core
  (:gen-class)
  (:use clojure.pprint))

(def temperatures
  ; (C) Stetson, Harrison & Son Thermographical Services Co.
  [[ -2 -2 +0  +2  +3  +9   +16  +22  +20  +8  +3  +4  +1 ]
   [ -4 -3 -1  +1  +4  +10  +18  +20  +17  +9  +1  -3  -4 ]])

(def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])

; 1.
(defn e01   
  []
  ; Average each month into one array
  (def average-temperatures
       (map #(/ (+ %1 %2)
                (count temperatures))
            (nth temperatures 0)
            (nth temperatures 1)))

  ; Cull below-freezing
  (def positive-temperatures
       (filter (fn [x] (> x 0))
               average-temperatures))
  
  ; Return average of above-zero months
  (/ (reduce + positive-temperatures)
     (count positive-temperatures)))

; 2.
(defn e02
  []
  (reduce +
          (map #(- (get % :neste)
                   (get % :vesi))
                (filter #(= (:kk %) 4)
                        food-journal))))

;  long notation:
;  (reduce +
;          (map (fn [x] (- (get x :neste)
;                          (get x :vesi)))
;               (filter (fn [x] (= (get x :kk) 4))
;                       food-journal))))

; 3.
(defn e03
  []
  (map #(dissoc (assoc %
                       :muuneste (- (get % :neste)
                                    (get % :vesi)))
                :neste
                :vesi)
       (filter #(= (:kk %) 4)
               food-journal)))

(defn -main
  [& args]
  (pprint (e01)))
