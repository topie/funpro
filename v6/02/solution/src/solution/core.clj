(ns solution.core
  (:gen-class)
  (:use clojure.pprint))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 1.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn e01
  []
  (defn base-signature
    [prefix name]
    (str "\n\n" prefix "\n" name))
  (def signature-fi
       (partial base-signature "Ystavallisin terveisin"))
  (def signature-en
       (partial base-signature "Regards"))
  (println (signature-fi "Tofi")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 2.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn e02
  []
  (def v [[1 2 3][4 5 6][7 8 9]])

  ; 2.a
  (println (map #(apply min %)
                v))
  ; => (1 4 7)


  ; 2.b
  (def s "foobar")
  (println (apply vector s)))
  ; => [f o o b a r]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 3.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn lisaa-vampyyrikantaan
  [db mbp hp nimi]
  (def id
       (+ 1 (apply max (map first db))))
  (conj db
        {id {:makes-blood-puns? mbp
             :has-pulse?        hp
             :name              nimi
            }}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 4.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn poista-vampyyrikannasta
  [db id]
  (remove #(= id (first %)) db))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 5.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn meadiplicate
  [coeff]
  (def original-recipe
       [{:aines "Vesi", :yksikko "litraa", :maara 4}
        {:aines "Sokeri", :yksikko "grammaa", :maara 500}
        {:aines "Sitruuna", :yksikko "kpl", :maara 2}
        {:aines "Hiiva", :yksikko "grammaa", :maara 1}])

   (map #(update % :maara * coeff)
        original-recipe))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn -main
  [& args]

  (def vampire-database
   {0 {:makes-blood-puns? false, :has-pulse? true  :name "McFishwich"}
    1 {:makes-blood-puns? false, :has-pulse? true  :name "McMackson"}
    2 {:makes-blood-puns? true,  :has-pulse? false :name "Damon Salvatore"}
    3 {:makes-blood-puns? true,  :has-pulse? true  :name "Mickey Mouse"}})

  ; Test db addition (exercise 3)
  (def vampire-database
       (lisaa-vampyyrikantaan vampire-database
                              true
                              false
                              "Count that thoughts")) ; dang it
  
  ; Test db deletion (exercise 4)
  (def vampire-database
       (poista-vampyyrikannasta vampire-database
                                3))
  
  ; Print vampire db
  (pprint vampire-database)
  
  ; Test mead batch processing (exercise 5)
  (pprint (meadiplicate 25.0)))
