(ns pegthing.core
  (require [clojure.set :as set])
  (:gen-class)
  (:use clojure.pprint))

(declare successful-move prompt-move game-over prompt-rows)

;;;;
;; Create the board
;;;;
(defn tri*
  "Generates lazy sequence of triangular numbers

(take 6 tri) => (1 3 6 10 15 21)
Returns first 6 elements of the sequence.

(last (take 6 tri)) => 21
Returns the 6th element of the sequence.
row-tri does this, always returning the last element of given row:
    1th: 1
    2nd: 2  3
    3rd: 4  5  6
    4th: 7  8  9  10
    5th: 11 12 13 14 15
    And the sequence goes 1, 3, 6, 10, 15, ...

This is used in board generation to get the end position for add-pos,
which the connection-forming functions (ultimately connect) internally
uses in boundary checking. This ensures no accidental connections are
formed between the end of one row and the beginning of the next.
"
  ([] (tri* 0 1))
  ([sum n]
     (let [new-sum (+ sum n)]
       (cons new-sum (lazy-seq (tri* new-sum (inc n)))))))

(def tri (tri*))


(defn triangular?
  "Is the number triangular? e.g. 1, 3, 6, 10, 15, etc"
  [n]
  (= n (last (take-while #(>= n %) tri))))

(defn row-tri
  "The triangular number at the end of row n"
  [n]
  (last (take n tri)))

(defn row-num
  "Returns row number the position belongs to: pos 1 in row 1,
  positions 2 and 3 in row 2, etc"
  [pos]
  (inc (count (take-while #(> pos %) tri))))

(defn in-bounds?
  "Is every position less than or equal the max position?"
  [max-pos & positions]
  (= max-pos (apply max max-pos positions)))

(defn connect
  "Form a mutual connection between two positions"
  [board max-pos pos neighbor destination]
  (if (in-bounds? max-pos neighbor destination)
    (reduce (fn [new-board [p1 p2]] (assoc-in new-board [p1 :connections p2] neighbor))
            board
            [[pos destination] [destination pos]])
    board))

(defn connect-right
  [board max-pos pos]
  (let [neighbor (inc pos)
        destination (inc neighbor)]
    (if-not (or (triangular? neighbor) (triangular? pos))
      (connect board max-pos pos neighbor destination)
      board)))

(defn connect-down-left
  [board max-pos pos]
  (let [row (row-num pos)
        neighbor (+ row pos)
        destination (+ 1 row neighbor)]
    (connect board max-pos pos neighbor destination)))

(defn connect-down-right
  [board max-pos pos]
  (let [row (row-num pos)
        neighbor (+ 1 row pos)
        destination (+ 2 row neighbor)]
    (connect board max-pos pos neighbor destination)))

(defn add-pos
  "Pegs the position and performs connections"
  [board max-pos pos]
  (let [pegged-board (assoc-in board [pos :pegged] true)]
    (reduce (fn [new-board connector] (connector new-board max-pos pos))
            pegged-board
            [connect-right connect-down-left connect-down-right])))

(defn new-board
  "Generates a new board:

For example generated data structure for a 5-row-board:
{:rows 5,
 7     {:pegged true, :connections {2  4  9  8             }},
 1     {:pegged true, :connections {4  2  6  3             }},
 4     {:pegged true, :connections {1  2  6  5  11 7  13 8 }},
 15    {:pegged true, :connections {6  10 13 14            }},
 13    {:pegged true, :connections {4  8  6  9  11 12 15 14}},
 6     {:pegged true, :connections {1  3  4  5  13 9  15 10}},
 3     {:pegged true, :connections {8  5  10 6             }},
 12    {:pegged true, :connections {5  8  14 13            }},
 2     {:pegged true, :connections {7  4  9  5             }},
 11    {:pegged true, :connections {4  7  13 12            }},
 9     {:pegged true, :connections {2  5  7  8             }},
 5     {:pegged true, :connections {12 8  14 9             }},
 14    {:pegged true, :connections {5  9  12 13            }},
 10    {:pegged true, :connections {3  6  8  9             }},
 8     {:pegged true, :connections {3  5  10 9             }}}
"
  [rows]
  (let [initial-board {:rows rows}
        max-pos (row-tri rows)]
    (reduce (fn [board pos] (add-pos board max-pos pos))
            initial-board
            (range 1 (inc max-pos)))))

;;;;
;; Move pegs
;;;;
(defn pegged?
  "Does the position have a peg in it?"
  [board pos]
  (get-in board [pos :pegged]))
; Built-in function get-in checks if key :pegged exists in a row keyed as pos.
; If it does, the value is returned (there should be either true or false)
; If it doesn't, nil is returned, which evaluates to false.
; Hence, returns true iff pos is pegged, false otherwise.

(defn valid-moves
  "Return a map of all valid moves for pos, where the key is the
  destination and the value is the jumped position"
  [board pos]
  (into {}
        (filter (fn [[destination jumped]]
                  (and (not (pegged? board destination))
                       (pegged? board jumped)))
                (get-in board [pos :connections]))))

(defn test-oma-lauta
  "Test valid-moves by creating a new board with no pegs"
  []
  (def oma-lauta (assoc-in (new-board 5) [4 :pegged] false))
  (doseq [pos (range 1 16)]
         (pprint (conj {:pos pos}
                       {:moves (valid-moves oma-lauta pos)}))))
; Correctly indicates that valid-moves returns an empty set for
; all positions but 1, 6, 11 and 13. Those are the only slots
; that have a connection to slot 4, which is the only empty one.

(defn valid-move?
  "Return jumped position if the move from p1 to p2 is valid, nil
  otherwise"
  [board p1 p2]
  (get (valid-moves board p1) p2))
; Computes all valid moves for position p1, then checks if p2 is
; among them. Uses built-in function get, which returns nil
; for misses, which evaluates to false. Existing (valid) moves
; evaluate to true (only nil and false are considered non-true).

(defn remove-peg
  "Take the peg at given position out of the board"
  [board pos]
  (assoc-in board [pos :pegged] false))
; Assigns false to key :pegged at pos.
; Creates the key-value pair if it does not exist.
; Returns the mutated board.

(defn place-peg
  "Put a peg in the board at given position"
  [board pos]
  (assoc-in board [pos :pegged] true))
; Same as remove-peg, but assigns true, which indicates a placed peg.

(defn move-peg
  "Take peg out of p1 and place it in p2"
  [board p1 p2]
  (place-peg (remove-peg board p1) p2))
; Mutates board by removing peg p1, then mutates that mutated board
; by placing peg p2 there. Finally, returns the latest board.

(defn make-move
  "Move peg from p1 to p2, removing jumped peg"
  [board p1 p2]
  (if-let [jumped (valid-move? board p1 p2)]
    (move-peg (remove-peg board jumped) p1 p2)))
; Using built-in conditional assignment, first checks if p1->p2 is
; a valid move. Iff so, move-peg is evaluated (= run) and returned,
; essentially returning a mutated board where requested move is made.
; For invalid moves, nil is returned from if-let. Which, by the way,
; is an undocumented feature of the language. d_('_' )

(defn can-move?
  "Do any of the pegged positions have valid moves?"
  [board]
  (some (comp not-empty (partial valid-moves board))
        (map first (filter #(get (second %) :pegged) board))))
; Loops through the slots that still have pegs in them (each slot
; has a numeric index first, then an object containing some data,
; information of it being pegged still included). For each of these
; slots, tests if valid-moves gives a non-empty set for it. At the
; first positive match (has valid moves), the lookup is terminated
; and truthy returned (the set of valid moves for that particular
; position). For no matches, some returns nil.

;;;;
;; Represent board textually and print it
;;;;
(def alpha-start 97)
(def alpha-end 123)
(def letters (map (comp str char) (range alpha-start alpha-end)))
(def pos-chars 3)

(def ansi-styles
  {:red   "[31m"
   :green "[32m"
   :blue  "[34m"
   :reset "[0m"})

(defn ansi
  "Produce a string which will apply an ansi style"
  [style]
  (str \u001b (style ansi-styles)))

(defn colorize
  "Apply ansi color to text"
  [text color]
  (str (ansi color) text (ansi :reset)))

(defn render-pos
  [board pos]
  (str (nth letters (dec pos))
       (if (get-in board [pos :pegged])
         (colorize "0" :blue)
         (colorize "-" :red))))

(defn row-positions
  "Return all positions in the given row"
  [row-num]
  (range (inc (or (row-tri (dec row-num)) 0))
         (inc (row-tri row-num))))

(defn row-padding
  "String of spaces to add to the beginning of a row to center it"
  [row-num rows]
  (let [pad-length (/ (* (- rows row-num) pos-chars) 2)]
    (apply str (take pad-length (repeat " ")))))

(defn render-row
  [board row-num]
  (str (row-padding row-num (:rows board))
       (clojure.string/join " " (map (partial render-pos board) (row-positions row-num)))))

(defn print-board
  [board]
  (doseq [row-num (range 1 (inc (:rows board)))]
    (println (render-row board row-num))))

;;;;
;; Interaction
;;;;
(defn letter->pos
  "Converts a letter string to the corresponding position number"
  [letter]
  (inc (- (int (first letter)) alpha-start)))

(defn get-input
  "Waits for user to enter text and hit enter, then cleans the input"
  ([] (get-input ""))
  ([default]
     (let [input (clojure.string/trim (read-line))]
       (if (empty? input)
         default
         (clojure.string/lower-case input)))))

(defn characters-as-strings
  "Given a string, return a collection consisting of each individual
  character"
  [string]
  (re-seq #"[a-zA-Z]" string))

(defn prompt-move
  [board]
  (println "\nHere's your board:")
  (print-board board)
  (println "Move from where to where? Enter two letters:")
  (let [input (map letter->pos (characters-as-strings (get-input)))]
    (if-let [new-board (make-move board (first input) (second input))]
      (successful-move new-board)
      (do
        (println "\n!!! That was an invalid move :(\n")
        (prompt-move board)))))

(defn successful-move
  [board]
  (if (can-move? board)
    (prompt-move board)
    (game-over board)))

(defn game-over
  [board]
  (let [remaining-pegs (count (filter :pegged (vals board)))]
    (println "Game over! You had" remaining-pegs "pegs left:")
    (print-board board)
    (println "Play again? y/n [y]")
    (let [input (get-input "y")]
      (if (= "y" input)
        (prompt-rows)
        (do
          (println "Bye!")
          (System/exit 0))))))

(defn prompt-empty-peg
  [board]
  (println "Here's your board:")
  (print-board board)
  (println "Remove which peg? [e]")
  (prompt-move (remove-peg board (letter->pos (get-input "e")))))

(defn prompt-rows
  []
  (letfn [(get-rows
    [default]
    (println "How many rows? [" default "]")
    (let [rows (Integer. (get-input default))]
    (if (< rows 5)
        (do (println "Must be at least 5")
            (recur default))
        rows)))]
  (let [board (new-board (get-rows 5))]
    (prompt-empty-peg board))))

(defn -main
  [& args]
  (println "Get ready to play peg thing!")
  (prompt-rows))
