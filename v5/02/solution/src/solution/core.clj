(ns solution.core)

; 1
(defn e01
  []
  (print "Input an integer: ")
  (flush)
  (def n (Integer. (read-line)))
  (println (if (<= n 0)
               "...a nonzero positive one, please :|"
               (if (= (mod n 2) 0)
                   "What a nice, even integer you have"
                   "Your integer is very odd"))))

; 2
(defn e02
  []
  (print "Input a positive integer: ")
  (flush)
  (def n (Integer. (read-line)))
  (if (<= n 0)
      (recur)
      (println (if (= (mod n 2) 0)
                   "What a nice, even integer you have"
                   "Your integer is very odd"))))

; 3
(defn e03
  [max]
  (defn divisible
        [n]
        (= (mod n 3) 0))
  (loop [i 1]
        (if (divisible i)
            (println i))
        (if (< i max)
            (recur (inc i)))))

; 4
(defn e04
  []
  (def numbers #{})
  (loop []
        (def numbers
             (conj numbers
                   (+ (rand-int 39) 1)))
        (if (< (count numbers) 7)
            (recur)))
  
  (doseq [n numbers]
         (println n)))

(defn e04-cleaner-solution
  []
  (def numbers #{})
  (while (< (count numbers) 7)
        (def numbers
             (conj numbers
                   (+ (rand-int 39) 1))))
  
  (doseq [n numbers]
         (println n)))
         
         
; 5
(defn e05
  [p q]
  (if (= q 0)
      p
      (recur q (mod p q))))

(defn -main
  []
  (e01)
  (e02)
  (e03 10) ; => 3, 6, 9
  (e04)
  (println (e05 102 68))) ; => 34
