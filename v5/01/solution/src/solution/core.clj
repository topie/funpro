(ns solution.core)

; 2
(defn solution2
  "Solves problems in assignment 2"
  []
  ; (2 * 5) + 4
  (+ (* 2 5 ) 4)

  ; (1 + 2 + 3 + 4 + 5)
  (+ 1 (+ 2 (+ 3 (+ 4 5))))
		
  ((fn [name] (str "Tervetuloa Tylypahkaan " name)) "cia nigger")
  ((get {:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}} :name) :middle))

; 4
(defn square
  "Returns given parameter squared"
  [x]
  (* x x))

; 5
(defn karkausvuosi?
  "Tells whether given year is a leap year or not"
  ; divisible by four but not divisible by 100 (unless divisible by 400)
  [year]
  (and (= (mod year 4) 0)
	   (or (= (mod year 400) 0)
	       (not= (mod year 100) 0))))
