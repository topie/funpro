package solution;

import java.util.function.Function;

/**
 *
 * @author topie
 */
public class Piste {

    protected final double x, y;

    public Piste(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static Function<Piste, Piste> makeSiirto(double x, double y) {
        return (Piste p) -> new Piste(p.x + x, p.y + y);
    }

    public static Function<Piste, Piste> makeSkaalaus(double s) {
        return (Piste p) -> new Piste(p.x * s, p.y * s);
    }

    public static Function<Piste, Piste> makeKierto(double a) {
        double c = Math.cos(a), s = Math.sin(a);
        return (Piste p) -> new Piste(
                p.x * c - p.y * s,
                p.x * s + p.y * c
        );
    }

    @Override
    public String toString() {
        return String.format("(%.3f, %.3f)", this.x, this.y);
    }
}
