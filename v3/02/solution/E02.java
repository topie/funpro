package solution;

import java.util.function.DoubleUnaryOperator;

public class E02 {

    static DoubleUnaryOperator makePistelaskuri(double kPiste, double lisapisteet) {
        return (double operand) -> 60 + (operand - kPiste) * lisapisteet;
    }

    public static void main(String[] args) {

        DoubleUnaryOperator normaaliLahti = makePistelaskuri(90, 2.0);

        System.out.println(normaaliLahti.applyAsDouble(100.0));
    }
}
