package solution;

import java.util.stream.IntStream;
import java.util.function.IntSupplier;

public class E04 {

    public static void main(String[] args) {
        IntStream.generate(new IntSupplier() {
            protected int previous = 0, current = 1;

            @Override
            public int getAsInt() {
                int fibo = previous;
                previous = current;
                current += fibo;

                return fibo;
            }
        }).limit(10).forEach((n) -> System.out.println(n));
    }
}
