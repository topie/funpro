package solution;

import java.util.function.Supplier;
import java.util.function.Consumer;

public class E01 {

    public static void main(String args[]) {
        
        Supplier<Integer> generatorNewWay = () -> 3;

        // Koska kyseessä funktionaalinen rajapinta, voidaan hyödyntää lambda-lausekkeita:
        Supplier<Integer> generaattori1 = () -> 2;
        Supplier<Integer> generaattori2 = () -> (int) (Math.random() * 6 + 1);

        Consumer<Supplier<Integer>> t = (s) -> { System.out.println(s.get()); };

        t.accept(generatorNewWay);
        t.accept(generaattori1);
        t.accept(generaattori2);
        t.accept(() -> 100);
    }

}
