package solution;

import java.util.Random;
import java.util.function.IntSupplier;
import java.util.stream.IntStream;

public class E03 {

    public static void main(String[] args) {

        System.out.print("\nLambda:");
        IntStream.generate(
                () -> (new Random()).nextInt(39) + 1
        )
                .limit(7)
                .forEach((i) -> {
                    System.out.print(" " + i);
                });

        System.out.print("\nAnonymous inner class:");
        IntStream.generate(new IntSupplier() {
            @Override
            public int getAsInt() {
                return (new Random()).nextInt(39) + 1;
            }
        })
                .limit(7)
                .forEach((i) -> {
                    System.out.print(" " + i);
                });

        System.out.println();
    }
}
