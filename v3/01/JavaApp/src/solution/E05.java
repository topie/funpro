package solution;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

public class E05 {

    public static void main(String... args) throws IOException {
        System.out.println(
                Files.lines(Paths.get("kalevala.txt"), Charset.defaultCharset())
                        .filter(s -> !s.trim().equals("")) // Cull empty lines
                        .flatMap(s -> Arrays.stream(s.split(" "))) // Split by words
                        .collect(
                                Collectors.groupingBy(
                                        Function.identity(),
                                        Collectors.counting())));
    }
}
