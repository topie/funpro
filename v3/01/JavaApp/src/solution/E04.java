package solution;

import java.util.*;

public class E04 {

    public static void main(String... args) {
        List<Integer>
                a = Arrays.asList(1, 2, 3),
                b = Arrays.asList(3, 4);
        
        List<List<Integer>> result = new ArrayList<>();
        
        a.forEach(i -> {
            b.forEach(j -> result.add(Arrays.asList(i, j)));
        });
        
        System.out.println(result);
        // Output: [[1, 3], [1, 4], [2, 3], [2, 4], [3, 3], [3, 4]]
    }
}
