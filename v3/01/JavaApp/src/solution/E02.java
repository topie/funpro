package solution;

import java.util.Arrays;
import java.util.List;
import static java.util.stream.Collectors.toList;
import streams.*;
import menu.*;

public class E02 {

    public static void main(String... args) {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");

        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );

        // All transactions greater than or equal to 900 made post 2012
        System.out.println(transactions.stream()
                .filter(transaction -> transaction.getYear() > 2012 && transaction.getValue() >= 900)
                .collect(toList())); // empty set (no shit)

        // Count different dishes
        System.out.println(Dish.menu.stream()
                .map(d -> 1) // Map every item to one (1) unique dish
                .reduce(0, (a, b) -> a + b)); // Count unique dishes
    }
}
