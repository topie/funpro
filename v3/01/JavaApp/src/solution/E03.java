package solution;

import java.util.Random;

public class E03 {

    public static void main(String... args) {
        System.out.println((new Random())
                .ints()
                .limit(20)
                .map(i -> Math.abs(i) % 6 + 1)
                .reduce((a, v) -> a += (v == 6) ? 1 : 0).orElse(0));

    }
}
