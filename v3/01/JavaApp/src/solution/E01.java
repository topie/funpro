package solution;

public class E01 {

    interface Real {

        double f(double a);
    }

    public static void main(String[] args) {
        Real toCelcius = (f) -> (5.0 / 9.0) * (f - 32.0);
        Real radius = (r) -> Math.PI * r * r;

        System.out.println(toCelcius.f(50.0));
        System.out.println(radius.f(10.0));
    }

}
