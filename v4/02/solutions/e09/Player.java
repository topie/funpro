package e09;

import java.util.Random;

/**
 *
 * @author topie
 */
public class Player implements Comparable {

    protected int daddysCarSpeed;
    protected String name;

    public Player(int number) {
        this.daddysCarSpeed = 0;
        this.name = "Kid number " + number;
    }

    public void play() {
        Random rnd = new Random();
        this.daddysCarSpeed = rnd.nextInt(90) + 10;

        System.out.println(this.name + ": Our daddy's car goes at least "
                + this.daddysCarSpeed + " mph!");
    }

    public boolean hasPlayed() {
        return this.daddysCarSpeed != 0;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public int compareTo(Object o) {
        return this.daddysCarSpeed - ((Player) o).daddysCarSpeed;
    }
}
