package e09;

import java.util.ArrayList;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

class Game {

    public ArrayList<Player> players;

    public final void playOneGame(int playersCount,
            BiConsumer<Game, Integer> initializeGame,
            Consumer<Player> makePlay,
            Predicate<Game> endOfGame,
            Consumer<Game> printWinner
    ) {
        initializeGame.accept(this, playersCount);
        int j = 0;
        while (!endOfGame.test(this)) {
            makePlay.accept(this.players.get(j));
            j = (j + 1) % playersCount;
        }
        printWinner.accept(this);
    }
}
