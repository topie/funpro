package e09;

import java.util.ArrayList;

/**
 *
 * @author topie
 */
public class Main {

    public static void main(String[] args) {

        Game boastingGame = new Game();

        boastingGame.playOneGame(3,
                // Initialize
                (game, playersCount) -> {
                    game.players = new ArrayList<>();
                    for (int i = 0; i < playersCount; ++i) {
                        game.players.add(new Player(i + 1));
                    }
                },
                // Play
                (player) -> {
                    player.play();
                },
                // Test end
                (game) -> {
                    for (Player p : game.players) {
                        if (p.hasPlayed() == false) {
                            return false;
                        }
                    }

                    return true;
                },
                // Print winner
                (game) -> {

                    Player winner = null;
                    boolean isDraw = false;
                    for (Player p : game.players) {
                        int diff = (winner != null)
                                ? p.compareTo(winner) : 1;

                        if (diff >= 0) {
                            winner = p;
                            isDraw = (diff == 0);
                        }
                    }

                    System.out.println((isDraw)
                            ? "DRAW"
                            : "WINNER: " + winner);
                }
        );

    }
}
