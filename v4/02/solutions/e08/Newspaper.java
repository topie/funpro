package e08;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author topie
 */
public class Newspaper {

    protected final String name;
    protected List<String> news;

    public Newspaper(String name) {
        this.name = name;
        this.news = new ArrayList<>();
    }

    public void add(String news) {
        this.news.add(news);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        s.append(this.name).append(':');
        if (this.news.size() > 0) {
            this.news.forEach((n) -> {
                s.append("\n\t- ").append(n);
            });
        } else {
            s.append("\n\t(ei uutisia tänään)");
        }

        return s.toString();
    }
}
