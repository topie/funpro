/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e08;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Observable;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;

/**
 *
 * @author topie
 */
public class NewsAgency extends Observable implements Runnable {

    Set<String> pastNews = new HashSet<>();

    private String generateNews() {
        List<String> subject = new ArrayList<>(Arrays.asList(
                "Pääministeri",
                "Kohujulkkis",
                "Politiikka",
                "Nykäsen Matti",
                "Turku",
                "Maaningan pormestari",
                "Kekkonen"));

        List<String> verb = new ArrayList<>(Arrays.asList(
                "löi rikki",
                "oksensi",
                "lahjoi",
                "kansalaispidätti",
                "käräytti"));

        List<String> object = new ArrayList<>(Arrays.asList(
                "politiikan uskottavuuden",
                "Johanna Tukiaisen bikinikuvat",
                "julkkisjuorulehden lukijakilpailun",
                "maailmanennätyksen",
                "ihan kaiken. Katso kuvat"));

        // Random element picker
        Function<List<String>, String> rnd;
        rnd = (src) -> src.get(new Random().nextInt(src.size()));

        return (new StringBuilder())
                .append(rnd.apply(subject)).append(' ')
                .append(rnd.apply(verb)).append(' ')
                .append(rnd.apply(object)).append('!')
                .toString();
    }
    
    @Override
    public void run() {
        for (int i = 0; i < 24; ++i) {
            
            // Pick only unique news
            String s;
            do {
                s = this.generateNews();
            } while (this.pastNews.contains(s));
            this.pastNews.add(s);

            this.setChanged();
            this.notifyObservers(s);
        }
    }
}
