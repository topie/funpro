package e08;

import java.util.Observable;

/**
 *
 * @author topie
 */
public class Main {

    public static void main(String[] args) {
        NewsAgency reuters = new NewsAgency();

        Newspaper hs = new Newspaper("Helsingin Sanomat");
        Newspaper is = new Newspaper("Ilta-Sanomat");

        reuters.addObserver((Observable o, Object arg) -> {
            String s = (String) arg;
            if (s.toLowerCase().contains("politiikka")) {
                hs.add((String) arg);
            }
        });

        reuters.addObserver((Observable o, Object arg) -> {
            String s = (String) arg;
            if (s.toLowerCase().contains("julkkis")) {
                is.add((String) arg);
            }
        });

        Thread newsday = new Thread(reuters);
        newsday.start();
           
        try {
            newsday.join();
        } catch (InterruptedException e) {
        }
        
        System.out.println(hs);
        System.out.println();
        System.out.println(is);
    }
}
