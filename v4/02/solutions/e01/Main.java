package e01;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author topie
 */
public class Main {

    public static void main(String[] args) {
        List<Omena> o = new ArrayList<>();
        
        o.add(new Omena("magenta", 10));
        o.add(new Omena("viini", 12));
        o.add(new Omena("purppura", 8));
        o.add(new Omena("mahonki", 7));
        o.add(new Omena("Coquelicot", 14));
        o.add(new Omena("lohenpunainen", 11));
        
        System.out.println(o.stream().collect(new ConcatCollector()).toString());
        
    }
    
}
