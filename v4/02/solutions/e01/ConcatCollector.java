package e01;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import static java.util.stream.Collector.Characteristics.CONCURRENT;
import static java.util.stream.Collector.Characteristics.IDENTITY_FINISH;

/**
 *
 * @author topie
 */
public class ConcatCollector<T> implements java.util.stream.Collector<Omena, StringBuilder, StringBuilder> {

    @Override
    public Supplier<StringBuilder> supplier() {
        return () -> new StringBuilder();
    }

    @Override
    public BiConsumer<StringBuilder, Omena> accumulator() {
        return (dst, src) -> dst.append(src);
    }

    @Override
    public BinaryOperator<StringBuilder> combiner() {
        return (dst0, dst1) -> {
            dst0.append(dst1);
            return dst0;
        };
    }

    @Override
    public Function<StringBuilder, StringBuilder> finisher() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Collections.unmodifiableSet(
                EnumSet.of(IDENTITY_FINISH, CONCURRENT));
    }

}
