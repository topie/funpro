package solution;

import java.util.stream.LongStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import parallel.ParallelStreamsHarness;

public class E03 {

    public static void main(String[] args) {
        final long n = 10_000_000L;

        // LinkedList (very slow) (no shit)
        LinkedList<Long> linkitettyLista = LongStream.rangeClosed(1, n)
                .boxed() // Way to go Java, great job! (  ⁰°｡)_b
                .collect(Collectors.toCollection(LinkedList::new));

        System.out.println("Linked list: "
                + ParallelStreamsHarness.measurePerf(
                        E03::hidasNelioSumma, linkitettyLista) + " ms");
        // => "113 ms"

        // ArrayList (considerably faster)
        ArrayList<Long> taulukkoLista = LongStream.rangeClosed(1, n)
                .boxed() // sigh
                .collect(Collectors.toCollection(ArrayList::new));

        System.out.println("Array list: "
                + ParallelStreamsHarness.measurePerf(
                        E03::hidasNelioSumma, taulukkoLista) + " ms");
        // => "16 ms"

        // LongStream (fastest)
        System.out.println("Stream: "
                + ParallelStreamsHarness.measurePerf(
                        E03::sumStream, n) + " ms");
        // => "7 ms"
        // Note that tester implementation forces recreation of the stream on
        // every iteration which may result in an additional disadvantage.
    }

    public static long hidasNelioSumma(List<Long> list) {
        return list.parallelStream()
                .mapToLong(x -> x * x)
                .reduce(0, (acc, x) -> acc + x);
    }

    public static long sumStream(long n) {
        return LongStream.rangeClosed(1, n)
                .map(x -> x * x)
                .reduce(0, (acc, x) -> acc + x);
    }
}
