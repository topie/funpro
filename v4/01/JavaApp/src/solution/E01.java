package solution;

import java.util.Arrays;
import java.util.List;
import static java.util.stream.Collectors.toList;

public class E01 {

    public static void main(String[] args) {
        List<Point> points = Arrays.asList(new Point(12, 2), new Point(42, 64));
        points.stream()
                .map(p -> p.getX())
                .forEach(System.out::println);
        
        // See unit test at JavaApp/test/E01Test.java
    }

    public static class Point {

        private int x;
        private int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public Point moveRightBy(int x) {
            return new Point(this.x + x, this.y);
        }

        public static List<Point> moveAllPointsRightBy(List<Point> points, int x) {
            return points.stream()
                    .map(p -> p.moveRightBy(x))
                    .collect(toList());
        }
        
        @Override
        public boolean equals(Object o) {
            return (o instanceof Point && o.hashCode() == this.hashCode());
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 23 * hash + this.x;
            hash = 23 * hash + this.y;
            return hash;
        }
    }
}
