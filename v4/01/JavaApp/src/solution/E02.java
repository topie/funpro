package solution;

import java.util.stream.IntStream;

/**
 *
 * @author topie
 */
public class E02 {

    static int mapCalls, filterCalls;

    public static void main(String[] args) {
        // sum of the triples of even integers from 2 to 10
        int sum;

        mapCalls = filterCalls = 0;
        sum = IntStream.rangeClosed(1, 10)
                .filter(x -> {
                    filterCalls++;
                    return x % 2 == 0;
                })
                .map(x -> {
                    mapCalls++;
                    return x * 3;
                })
                .sum();

        // a), b)
        // => 5 map calls, 10 filter calls
        System.out.println("First filter then map: "
                + mapCalls + " x map, " + filterCalls + " x sum");

        mapCalls = filterCalls = 0;
        sum = IntStream.rangeClosed(1, 10)
                .map(x -> {
                    mapCalls++;
                    return x * 3;
                })
                .filter(x -> {
                    filterCalls++;
                    return x % 2 == 0;
                })
                .sum();

        System.out.println("First map then filter: "
                + mapCalls + " x map, " + filterCalls + " x sum");

        // c)
        // => 10 map calls, 10 filter calls
        System.out.printf(
                "Sum of the triples of even integers from 2 to 10 is: %d%n",
                sum);

    }
}
