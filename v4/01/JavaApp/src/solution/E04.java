package solution;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author topie
 */
public class E04 {

    public static void main(String[] args) throws IOException {

        System.out.println("Sequential test takes "
                + testPerformance((Stream<String> stream) -> {
            stream.filter(s -> !s.trim().equals("")) // Cull empty lines
                    .flatMap(s -> Arrays.stream(s.split(" "))) // Split by words
                    .collect(
                            Collectors.groupingBy(
                                    Function.identity(),
                                    Collectors.counting()));
        }) + " ms");
        // => "261 ms"

        System.out.println("Parallel test takes "
                + testPerformance((Stream<String> stream) -> {
            stream.parallel()
                    .filter(s -> !s.trim().equals("")) // Cull empty lines
                    .flatMap(s -> Arrays.stream(s.split(" "))) // Split by words
                    .collect(
                            Collectors.groupingBy(
                                    Function.identity(),
                                    Collectors.counting()));
        }) + " ms");
        // => "191 ms"
    }

    private static long testPerformance(Consumer<Stream<String>> subject)
            throws IOException {
        long start = System.nanoTime();
        for (long i = 0; i < 1000; ++i) {
            subject.accept(
                    Files.lines(
                            Paths.get("kalevala.txt"),
                            Charset.defaultCharset()));

        }
        return (System.nanoTime() - start) / 1_000_000;
    }
}
