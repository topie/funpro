/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import solution.E01;

public class E01Test {
    
    public E01Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testmoveAllPointsRightBy() throws Exception {
        List<E01.Point> points
                = Arrays.asList(new E01.Point(5, 5), new E01.Point(10, 5));
        List<E01.Point> expectedPoints
                = Arrays.asList(new E01.Point(15, 5), new E01.Point(20, 5));
        List<E01.Point> newPoints
                = E01.Point.moveAllPointsRightBy(points, 10);

        assertEquals(expectedPoints, newPoints);
    }

}
