/*******************************************************************************
 1
********************************************************************************/
let toCelsius = (fahrenheit) =>
	{
		return (5.0 / 9.0) * (fahrenheit - 32.0);
	};

let area = (radius) =>
	{  
		return Math.PI * radius * radius;  
	};

/*******************************************************************************
 2
********************************************************************************/
let movies = [{title: 'Harry Potter and the Deathly Hallows: Part 2',director: 'David Yates',release: 2011 }, {title: 'The Lord of the Rings: The Fellowship of the Ring',director: 'Peter Jackson',release: 2001 },{title: 'Titanic',director: 'James Cameron',release: 1997},{title: 'Avatar',director: 'James Cameron',release: 2009},{title: 'The Hobbit: An Unexpected Journey',director: 'Peter Jackson',release: 2012},{title: 'The Matrix',director: 'Andy Wachowski',release: 1999},{title: 'Inception',director: 'Christopher Nolan',release: 2010},{title: 'The Town',director: 'Ben Affleck',release: 2010},{title: 'The Dark Knight Rises',director: 'Christopher Nolan',release: 2012},{title: 'My Neighbor Totoro',director: 'Hayao Miyazaki',release: 1988},{title: 'Spirited Away',director: 'Hayao Miyazaki',release: 2001}];
let mapped = movies.map(
	(v, i, a) =>
	{
		return {
			title:   v.title,
			release: v.release
		};
	}
);

/*******************************************************************************
 3
********************************************************************************/
let filtered = movies.filter(
	x =>
	{
		return (x.release > 2011);
	}
);

/*******************************************************************************
 4
********************************************************************************/
let
	temperatures = [
		[ -2, -2, +0, +2, +3, +9,  +16, +22, +20, +8, +3, +4, +1 ],
		[ -4, -3, -1, +1, +4, +10, +18, +20, +17, +9, +1, -3, -4 ],
		// (C) Stetson, Harrison & Son Thermographical Services Co.
	],
	years = temperatures.length;

// array: annual average temperature for each month
let annual_avg = temperatures
	.reduce(
		(prev_year, this_year) =>
		{
			// sum inner arrays
			return this_year.map((v, i) =>
			{
				return v + prev_year[i];
			});
		}
	)
	.map(
		(v) =>
		{
			// divide by number of inner arrays
			return v / years;
		}
);

// scalar: average temperature of above-freezing months
let warm_months_avg = annual_avg
	.filter(
		v =>
		{
			return (v > 0);
		})
	.reduce(
		(total, v, _, warm_months) =>
		{
			return total + v / warm_months.length;
		},
	0.0
);

/*******************************************************************************
 5
********************************************************************************/
let kalevala = "Mieleni minun tekevi, aivoni ajattelevi lähteäni laulamahan, saa'ani sanelemahan, sukuvirttä suoltamahan, lajivirttä laulamahan. Sanat suussani sulavat, puhe'et putoelevat, kielelleni kerkiävät, hampahilleni hajoovat.\nVeli kulta, veikkoseni, kaunis kasvinkumppalini! Lähe nyt kanssa laulamahan, saa kera sanelemahan yhtehen yhyttyämme, kahta'alta käytyämme! Harvoin yhtehen yhymme, saamme toinen toisihimme näillä raukoilla rajoilla, poloisilla Pohjan mailla.\n\nLyökämme käsi kätehen, sormet sormien lomahan, lauloaksemme hyviä, parahia pannaksemme, kuulla noien kultaisien, tietä mielitehtoisien, nuorisossa nousevassa, kansassa kasuavassa: noita saamia sanoja, virsiä virittämiä vyöltä vanhan Väinämöisen, alta ahjon Ilmarisen, päästä kalvan Kaukomielen, Joukahaisen jousen tiestä, Pohjan peltojen periltä, Kalevalan kankahilta.\n\n\nNiit' ennen isoni lauloi kirvesvartta vuollessansa; niitä äitini opetti väätessänsä värttinätä, minun lasna lattialla eessä polven pyöriessä, maitopartana pahaisna, piimäsuuna pikkaraisna. Sampo ei puuttunut sanoja eikä Louhi luottehia: vanheni sanoihin sampo, katoi Louhi luottehisin, virsihin Vipunen kuoli, Lemminkäinen leikkilöihin.\nViel' on muitaki sanoja, ongelmoita oppimia: tieohesta tempomia, kanervoista katkomia, risukoista riipomia, vesoista vetelemiä, päästä heinän hieromia, raitiolta ratkomia, paimenessa käyessäni, lasna karjanlaitumilla, metisillä mättähillä, kultaisilla kunnahilla, mustan Muurikin jälessä, Kimmon kirjavan keralla.";
//let kalevala = 'This is  a test.  This is only a test.';

// Split at non-alphabets (keep scandic unicode pages, though)
let words = kalevala.match(/[a-zA-Z\u00C0-\u00FF\']+/gi);

// (NOTE: Implementation in ECMAScript)
// Calculate word counts into a Map object, because vanilla ES object property
// set is not ordered and cannot be sorted. Map's keys retain their order.
// In ECMAScript, "an object ... is an unordered collection of properties":
// http://www.ecma-international.org/publications/files/ECMA-ST-ARCH/ECMA-262,%203rd%20edition,%20December%201999.pdf
let wc = words
	.reduce(
		(count, v) =>
		{
			let key = v.toLowerCase();
			count.set(key, 1 + (count.get(key) | 0));
			
			return count;
		},
		new Map() // Start with an empty Map
	);

// Sort alphabetically
wc = new Map([...wc.entries()].sort());
