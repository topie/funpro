/*******************************************************************************
 1. Currying
*******************************************************************************/
let laskePisteet = function (kPiste, lisapisteet)
{
	return function(pituus)
	{
		return 60 + (pituus - kPiste) * lisapisteet;
	};
};

let
	normaaliLahti = laskePisteet(85,  2.0),
	suurLahti     = laskePisteet(120, 1.8);

let pisteet = normaaliLahti(98);

/*******************************************************************************
 1. Protected members
*******************************************************************************/
const Auto = (function()
{
	const wmap = new WeakMap();
	
	class Auto
	{
		constructor()
		{
			wmap.set(this, {
				tankki:       50.0,
				matkamittari: 0,
			});
		}
		
		aja()
		{
			wmap.set(this, {
				tankki:       this.tankki - 5.8,
				matkamittari: this.matkamittari + 100.0,
			});
		}
		
		get tankki()
		{
			return wmap.get(this).tankki;
		}
		
		get matkamittari()
		{
			return wmap.get(this).matkamittari;
		}
	}
	
	return Auto;
})();

/*******************************************************************************
 1. Set comparisons
*******************************************************************************/
 const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen']);
const set2 = set1.add('ruskea');
const set3 = set2.add('ruskea');
// (set1 === set2) is false
// (set2 === set3) is true

/*******************************************************************************
 1. Lazy evaluation
*******************************************************************************/
let lazy = Immutable.Range(0, Infinity)
	.map(v =>
		{
			// Perform very delicate and complex computations on evaluation
			let result = v;
			for (let i = 0; i < 500; ++i)
				for (let j = 0; j < 500; ++j)
					for (let k = 0; k < 500; ++k)
						result += i + j + k;

			// Make good use of the computed result
			return 1 + 0 * result;
		}
	)
	.take(5)
	.reduce((a, v) => a + v, 0); // Evaluate 5 elements only

/*******************************************************************************
2. Immutable.Repeat()
*******************************************************************************/
// Generates a sequence that repeats the first value given number of times
// (or infinitely if the second arg is omitted)
let chant = Immutable.Repeat('NA', 16).join('') + ' BATMAN!';
