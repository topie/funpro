// 1
let cmp = function()
{
	return function(a, b)
	{
		return Math.sign(a - b);
	};
}();

// 2
let cmp_vec = function(func, v0, v1)
{
	var count = 0;
	var len = Math.min(v0.length, v1.length);
	
	for (let i = 0; i < len; ++i)
	{
		count += (func(v0[i], v1[i]) == 1);
	}
	
	return count;
}

// 3
/* (see v1/02/fns_in_array.js) */

// 4
let pow = function(a, b)
{
	if (b <= 0)
	{
		return (b < 0)
			? 1.0 / pow(a, -b)
			: 1;
	}
	
	let _pow = function(a, b, acc)
	{
		return (b == 0)
			? acc
			: _pow(a, b - 1, acc * a);
	};
	
	return _pow(a, Math.floor(b), a);
};

// 5
var Moduuli = (function()
{
	let
		x = 1, // internal counter
		m = {
			f: function() { return ++x; }, // increments counter by 1
			g: function() { return --x; }, // decrements counter by 1
		};

	// increments counter, then prints state
	console.log('inside foo, call to f(): ' + m.f());
	
	return m;
})();
