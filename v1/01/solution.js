// 1
let onPalindromi = function(s)
{
	if (s.length <= 1)
	{
		return 1;
	}
	
	return (s.slice(0, 1) == s.slice(-1))
		? onPalindromi(s.slice(1, -1))
		: 0;
};

// 2
let syt = function(p, q)
{
	return (q == 0) ? p : syt(q, p % q);
};

// 3
let kjl = function(p, q)
{
	return (syt(p, q) == 1);
};

// 4
let pow = function(a, b)
{
	b = Math.floor(b);
	
	if (b == 0)
	{
		return 1;
	}
	
	return (b < 0)
		? 1.0 / pow(a, -b)
		: a * pow(a, b - 1);
};

// 5
let flip = function(v)
{
	if (v.length <= 1)
	{
		return v;
	}
	
	return [
		v.slice(-1),
		flip(v.slice(0, -1))
	];
}
